# Resolución Pregunta 2

## La pregunta: 

> Utilizando el [Dockerfile](../PREGUNTA_N1/Dockerfile) anterior , elabore una forma de despliegue para actualizar el documento “index.html” sin necesidad de recrear el contenedor cada vez que este sea modificado.

## Prerrequisitos de instalación

Esta solución fue creada con:

    Docker 4.2.0

(De igual forma es necesario tener WSL2 instalado si el sistema es Windows).

## Comandos de Ejecución 

En el directorio de PREGUNTA_N1 ejecutar:
```shell
$ docker build -t pregunta_n1 .
```
#### (Básicamente tiene que estar creada la imagen anterior, para no repetir código, si ya existe es posible omitir este paso).

Con la imagen es posible desplegar un contenedor que cumpla la pregunta de dos formas:


La primera involucra un comando para asignar el archivo [index.html](../PREGUNTA_N1/index.html) 
```shell
$  docker run -it -v </ruta/del/archivo/index.html>:/usr/share/nginx/html/index.html --rm -d -p 8080:80 --name server_pregunta_n2 pregunta_n1
```
#### En sistemas Windows y utilizando Git Bash, es necesario agregar la línea  MSYS_NO_PATHCONV=1 para ejecutarse, más información en este [issue](https://github.com/docker-archive/toolbox/issues/673)

La segunda solución puede ser ejecutada desde el directorio PREGUNTA_N2:
```shell
$ docker compose up
```
#### Este comando mantendrá el contenedor abierto en la terminal

## Paso a paso de la solución

Se considera que la imagen que se crea con el [Dockerfile](../PREGUNTA_N1/Dockerfile) de la pregunta 1 ya existe (Si no, es necesario ejecutar docker build, más información en [README.md](../PREGUNTA_N1/README.md))

Para la solución 1, es simplemente encontrar la ruta del archivo [index.html](../PREGUNTA_N1/index.html) en la máquina local y entregarlo como parámetro luego de la opción -v en docker run luego de docker build.

Imagen de ejecución en máquina local: 

![Imagen_Sol1](https://dl.dropboxusercontent.com/s/wkh9vmvil7rjir4/pregunta2sol1.png)

Para la solución 2, la forma fue la siguiente: 

1. Se crea el archivo [docker-compose.yml](./docker-compose.yml)
2. Se edita el archivo [docker-compose.yml](./docker-compose.yml) con:

        version: '3'
        services:
        web:
            image: pregunta_n1
            volumes:
            - "../PREGUNTA_N1:/usr/share/nginx/html/"
            ports:
            - "8080:80"
    (La primera línea especifica la versión del archivo docker-compose. de la segunda línea empieza la declaración de los servicios, en donde el servidor web, tomará la imagen pregunta_n1, montará la carpeta donde está alojado el archivo [index.html](../PREGUNTA_N1/index.html) y abrirá los puertos 8080 para la ejecución)


3. Luego en la terminal se ejecuta el comando:

    ```shell
    $ docker compose up
    ```
    Este comando usará el archivo [docker-compose.yml](./docker-compose.yml) y procederá a crear el contenedor

6. Finalmente se comprueba la ejecución final en el navegador accediendo a localhost:8080

    ![Imagen_Sol2](https://dl.dropboxusercontent.com/s/13rfvkoxdsy8ag2/pregunta2sol2.png)

Nota: El archivo [.dockerignore](../PREGUNTA_N1/.dockerignore) es utilizado en la solución 2 dado que se utiliza la carpeta en vez del archivo html.







