# Resolución Pregunta 3

## La pregunta: 

> Despliegue un contenedor con MySQL, luego cree un usuario y contraseña, asegúrese de que tiene conectividad desde su equipo local a la instancia de BD. Adjunte el comando para crear este contenedor.

## Prerrequisitos de instalación

Esta solución fue creada con:

    Docker 4.2.0

(De igual forma es necesario tener WSL2 instalado si el sistema es Windows).

## Comando de despliegue del contenedor mysql. Primer paso

```shell
$ docker run -p 3306:3306 --name=mysql_pregunta_n3 -e MYSQL_ROOT_PASSWORD='test' -d mysql/mysql-server:latest
```

## Paso a paso de la posterior configuración

1. En una terminal se ejecuta:

    ```shell
    $ docker exec -it mysql_pregunta_n3 bash
    ```

2. Luego la terminal cambiará a la terminal presente en el contenedor donde ingresamos como usuario root:

    ```shell
    # mysql -u root -p
    ```
    Este comando pedirá una contraseña del usuario root. La contraseña del usuario root es 'test' pues se la asginamos como variable de entorno al inicio del despliegue con -e MYSQL_ROOT_PASSWORD='test'

3. En la interfaz de mysql, creamos un usuario que tenga privilegios tanto en la base de datos como de conexión con la siguiente línea:

    ```shell
    mysql> CREATE USER 'usuario' IDENTIFIED BY 'clave';
    ```

4. Finalmente, utilizando un gestor de BD en la máquina local, probamos la conexión al puerto 3306, que fue asignado al inicio con -p 3306:3306:

    En esta imagen se prueba la conexión con el programa DBeaver:
![Imagen_conexion_prueba](https://dl.dropboxusercontent.com/s/e2s5bbcazua0kj9/pregunta3.png)