# Resolución Pregunta 1

## La pregunta: 

> Desarrolle una receta Dockerfile que permita cargar y desplegar un index.html con el texto “¡Hello Zippy!”, el servicio debe poder ser accedido desde el puerto 8080.

## Prerrequisitos de instalación

Esta solución fue creada con:

    Docker 4.2.0

(De igual forma es necesario tener WSL2 instalado si el sistema es Windows).

## Comandos de Ejecución 

En el directorio de PREGUNTA_N1 ejecutar:
```shell
$ docker build -t pregunta_n1 .
```

#### (Es importante dejar el punto pues indica que el Dockerfile se encuentra en el directorio donde se ejecuta esta build).

Una vez creada la imagen pregunta_n1 ejecutar:
```shell
$ docker run -it --rm -d -p 8080:80 --name server pregunta_n1
```

## Paso a paso de la solución

1. Se crean los archivos [index.html](./index.html) y [Dockerfile](./Dockerfile)
2. Se edita el archivo [index.html](./index.html) con "!Hello Zippy!"
3. Se edita el archivo [Dockerfile](./Dockerfile) con:

        FROM nginx:latest
        COPY ./index.html /usr/share/nginx/html/index.html
    (La primera línea selecciona la imagen más reciente y oficial de ngnix presente en [Dockerhub](https://hub.docker.com/_/nginx). La segunda línea copia el archivo [index.html](./index.html) al directorio de la imagen oficial)
4. En una terminal se ejecuta el comando:
    
    ```shell
    $ docker build -t pregunta_n1 .
    ```
    Este comando construye la imagen "pregunta_n1" personalizada a partir del [Dockerfile](./Dockerfile) creado en el paso 3

5. Luego en la terminal se ejecuta el comando:

    ```shell
    $ docker run -it --rm -d -p 8080:80 --name server pregunta_n1
    ```
    Este comando ejecutará el contenedor "server" en el puerto 8080 a partir de la imagen pregunta_n1 donde las opciones --rm y -d harán que este contenedor se elimine al final de su ejecución.

6. Finalmente se comprueba la ejecución final en el navegador accediendo a localhost:8080

    ![Imagen_Final](https://dl.dropboxusercontent.com/s/djbhz5kf1k4lsks/pregunta1.png)







